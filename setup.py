#!/usr/bin/env python
"""
MBB plugin for MultiQC, adds modules for bioinformatic workflows reports.

For more information about MultiQC, see http://multiqc.info
"""

from setuptools import setup, find_packages

version = '0.3'

setup(
    name = 'mbb_mqc_plugin',
    version = version,
    author = '',
    author_email = '',
    description = "",
    long_description = __doc__,
    keywords = 'bioinformatics',
    url = '',
    download_url = '',
    license = '',
    packages = find_packages(),
    include_package_data = True,
    install_requires = [
        'multiqc'
    ],
    entry_points = {
        'multiqc.modules.v1': [
            'blast = mbb_mqc_plugin.modules.blast:MultiqcModule',
            'krona_iframe = mbb_mqc_plugin.modules.krona_iframe:MultiqcModule',
            'deepvariant_iframe = mbb_mqc_plugin.modules.deepvariant_iframe:MultiqcModule',
            'plot_vcf_dv_iframes = mbb_mqc_plugin.modules.plot_vcf_dv_iframes:MultiqcModule',
            'generic_iframe = mbb_mqc_plugin.modules.generic_iframe:MultiqcModule'
        ],
        'multiqc.cli_options.v1': [
            'disable_plugin = mbb_mqc_plugin.cli:disable_plugin'
        ],
        'multiqc.hooks.v1': [
            'execution_start = mbb_mqc_plugin.custom_code:mbb_mqc_plugin_execution_start'
        ]
    },
    classifiers = [
        'Intended Audience :: Science/Research',
        'Natural Language :: English',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: POSIX',
        'Operating System :: Unix',
        'Programming Language :: Python',
        'Programming Language :: JavaScript',
        'Topic :: Scientific/Engineering',
        'Topic :: Scientific/Engineering :: Bio-Informatics',
        'Topic :: Scientific/Engineering :: Visualization',
    ],
)

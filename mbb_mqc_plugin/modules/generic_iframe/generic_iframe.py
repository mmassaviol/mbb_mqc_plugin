#!/usr/bin/env python

""" mbb_mqc_plugin generic_iframe module """

from __future__ import print_function
from collections import OrderedDict
import logging

from multiqc import config
from multiqc.plots import linegraph
from multiqc.modules.base_module import BaseMultiqcModule

# Initialise the main MultiQC logger
log = logging.getLogger('multiqc')

class MultiqcModule(BaseMultiqcModule):

    def __init__(self):

        # Halt execution if we've disabled the plugin
        if config.kwargs.get('disable_plugin', True):
            return None

        # Initialise the parent module Class object
        super(MultiqcModule, self).__init__(
            name = 'generic',
            target = "generic",
            anchor = 'generic_iframe',
            href = '',
            info = "allows to insert html report inside an iframe."
        )

        # Find and load any input files for this module
  
        self.generic_iframe_generic_data = dict()
        line_plot_html = ''
        desc = 'This is an iframe of report :'
        for f in self.find_log_files('generic_iframe/generic_data'):
            self.generic_iframe_generic_data[f['s_name']] = dict()
            for l in f['f'].splitlines():
                key, value = l.split(None, 1)
                self.generic_iframe_generic_data[f['s_name']][key] = value
            for k, v in self.generic_iframe_generic_data[f['s_name']].items():
                line_plot_html = line_plot_html + ' <iframe src="' + v + '"   height="500px" width="100%" ></iframe> ' +'\n' 
                desc = desc + ", " +  v
        # Filter out samples matching ignored sample names
        #self.generic_iframe_generic_data = self.ignore_samples(self.generic_iframe_generic_data)

        log.info("Found {} reports".format(len(self.generic_iframe_generic_data)))

        if len(self.generic_iframe_generic_data) != 0:
            # Write parsed report data to a file
            self.write_data_file(self.generic_iframe_generic_data, 'multiqc_generic_iframe')

            # Add a report section with the line plot
            self.add_section(
                description = desc,
                helptext = '''
                 html files must be present in their paths relative to this report
                ''',
                plot = line_plot_html
            )

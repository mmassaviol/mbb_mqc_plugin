#!/usr/bin/env python

""" mbb_mqc_plugin blast plugin module """

from __future__ import print_function
from collections import OrderedDict
import logging
import re
import os
from multiqc import config
from multiqc.plots import table, linegraph
from multiqc.modules.base_module import BaseMultiqcModule
import urllib.request # test address availability


# Initialise the main MultiQC logger
log = logging.getLogger('multiqc')

class MultiqcModule(BaseMultiqcModule):

    def __init__(self):

        # Halt execution if we've disabled the plugin
        if config.kwargs.get('disable_plugin', True):
            return None

        # Initialise the parent module Class object
        super(MultiqcModule, self).__init__(
            name = 'Blast',
            target = "blast",
            anchor = 'blast',
            href = '',
            info = "Module to plot a table out of blast results"
        )

        
        self.sheaders = OrderedDict()
        #qseqid\tsseqid\tpident\tlength\tmismatch\tgapopen\tqstart\tqend\tsstart\tsend\tevalue\tbitscore\tqlen
        self.sheaders['qseqid'] = {
                        'title': 'QueryID',
                        'description': 'Query Seq-id',
                        'scale': False,
                        'format': '{:,.s}'
        }
        self.sheaders['sseqid'] = {
                        'title': 'sseqid',
                        'description': 'Subject Seq-id',
                        'scale': False,
                        'format': '{:,.s}'
        }
        self.sheaders['pident'] = {
                        'title': 'pident',
                        'description': 'Percentage of identical matches',
                        'scale': 'PuBu',
                        'format': '{:,.2f}'
        }
        self.sheaders['length'] = {
                        'title': 'length',
                        'description': 'Alignment length',
                        'scale': 'YIGn',
                        'format': '{:,.i}'
        }
        self.sheaders['mismach'] = {
                        'title': 'mismach',
                        'description': 'Number of mismatches',
                        'scale': 'OrRd',
                        'format': '{:,.i}'
        }
        self.sheaders['gapopen'] = {
                        'title': 'gapopen',
                        'description': 'Number of gap openings',
                        'scale': False,
                        'format': '{:,.i}'
        }
        self.sheaders['qstart'] = {
                        'title': 'qstart',
                        'description': 'Start of alignment in query',
                        'scale': False,
                        'format': '{:,.i}'
        }
        self.sheaders['qend'] = {
                        'title': 'qend',
                        'description': 'Enf of alignment in query',
                        'scale': False,
                        'format': '{:,.i}'
        }
        self.sheaders['sstart'] = {
                        'title': 'sstart',
                        'description': 'Start of alignment in subject',
                        'scale': False,
                        'format': '{:,.i}'
        }
        self.sheaders['evalue'] = {
                        'title': 'evalue',
                        'description': 'Expect value',
                        'scale': False,
                        'format': '{:,.7f}'
        }
        self.sheaders['bitscore'] = {
                        'title': 'bitscore',
                        'description': 'Bit score',
                        'scale': False,
                        'format': '{:,.i}'
        }
        self.sheaders['qlen'] = {
                        'title': 'qlen',
                        'description': 'Query sequence length',
                        'scale': 'YIGn',
                        'format': '{:,.i}'
        }



        self.blast_data = dict()
        for f in self.find_log_files('blast/blast_data'):
            try:
                self.blast_data.update(self.parse_blast_tab(f['f'], f['s_name']))
            except:
                log.error('Could not parse blast_data file in {}'.format(f['fn']))




        log.info("Found {} reports".format(len(self.blast_data)))

        # Write parsed report data to a file
        if (len(self.blast_data) > 0):
            self.write_data_file(self.blast_data, 'multiqc_blast')
        
            tconfig = {
                'id': 'blast_plot',
                'title': 'Blast: Results table'
            }
            table_plot_html = table.plot(self.blast_data,self.sheaders, tconfig)

            # Add a report section with the line plot
            self.add_section(
                description = 'This is a tabular output of blast',
                helptext = '''
                    TODO
                ''',
                plot = table_plot_html
            )

    def parse_blast_tab(self, file_contents, s_name):
        out_dict = dict()
        #qseqid\tsseqid\tpident\tlength\tmismatch\tgapopen\tqstart\tqend\tsstart\tsend\tevalue\tbitscore\tqlen
        fl = file_contents.splitlines()
        #var_lines = fl[fl.index("# Variant positions")+1:fl.index("# All positions (variant and fixed)")]
        headers = None
        hit=0
        for l in fl:
            if l.startswith("qseqid"):
                headers = l.split("\t")
            elif headers is not None:
                hit += 1
                cdict = OrderedDict()
                content = l.split("\t")
                for i in range(0, len(content)):
                    if i == 1 and "refseq" in s_name:
                        cdict[headers[i]] = '<a href="https://www.ncbi.nlm.nih.gov/refseq/?term='+ content[i]+ '" target="_blank">' + content[i] + '</a>'
                    else:    
                        cdict[headers[i]] = content[i]
                out_dict[hit] = cdict
        return out_dict        

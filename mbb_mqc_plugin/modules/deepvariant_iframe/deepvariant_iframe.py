#!/usr/bin/env python

""" mbb_mqc_plugin deepvariant_iframe module """

from __future__ import print_function
from collections import OrderedDict
import logging

from multiqc import config
from multiqc.plots import linegraph
from multiqc.modules.base_module import BaseMultiqcModule

# Initialise the main MultiQC logger
log = logging.getLogger('multiqc')

class MultiqcModule(BaseMultiqcModule):

    def __init__(self):

        # Halt execution if we've disabled the plugin
        if config.kwargs.get('disable_plugin', True):
            return None

        # Initialise the parent module Class object
        super(MultiqcModule, self).__init__(
            name = 'DeepVariant',
            target = "DeepVariant",
            anchor = 'deepvariant_iframe',
            href = 'https://github.com/google/deepvariant',
            info = "is an analysis pipeline that uses a deep neural network to call genetic variants from next-generation DNA sequencing data."
        )

        # Find and load any input files for this module  
        self.deepvariant_iframe_deepvariant_data = dict()
        line_plot_html = ''
        for f in self.find_log_files('deepvariant_iframe/deepvariant_data'):
            self.deepvariant_iframe_deepvariant_data[f['s_name']] = dict()
            for l in f['f'].splitlines():
                key, value = l.split(None, 1)
                self.deepvariant_iframe_deepvariant_data[f['s_name']][key] = value
            for k, v in self.deepvariant_iframe_deepvariant_data[f['s_name']].items():
                line_plot_html = line_plot_html + ' <iframe src="' + v + '"   height="400px" width="100%" ></iframe> ' +'\n' 

        # Filter out samples matching ignored sample names
        #self.deepvariant_iframe_deepvariant_data = self.ignore_samples(self.deepvariant_iframe_deepvariant_data)

        log.info("Found {} reports".format(len(self.deepvariant_iframe_deepvariant_data)))

        if len(self.deepvariant_iframe_deepvariant_data) != 0:
            # Write parsed report data to a file
            self.write_data_file(self.deepvariant_iframe_deepvariant_data, 'multiqc_deepvariant_iframe')


            # Add a report section with the line plot
            self.add_section(
                description = 'This is a visual summary of DeepVariant results',
                helptext = '''

                ''',
                plot = line_plot_html
            )

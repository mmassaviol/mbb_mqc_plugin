#!/usr/bin/env python

""" mbb_mqc_plugin plot_vcf_dv_iframes module """

from __future__ import print_function
from collections import OrderedDict
import logging

from multiqc import config
from multiqc.plots import linegraph
from multiqc.modules.base_module import BaseMultiqcModule

# Initialise the main MultiQC logger
log = logging.getLogger('multiqc')

class MultiqcModule(BaseMultiqcModule):

    def __init__(self):

        # Halt execution if we've disabled the plugin
        if config.kwargs.get('disable_plugin', True):
            return None

        # Initialise the parent module Class object
        super(MultiqcModule, self).__init__(
            name = 'Plot vcfs with DeepVariant',
            target = "Plot vcfs with DeepVariant",
            anchor = 'plot_vcf_dv_iframes',
            href = 'https://github.com/google/deepvariant',
            info = "Use DeepVariant to plot stats about variant files"
        )

        # Find and load any input files for this module  
        self.plot_vcf_dv_iframes_plot_vcf_dv_iframes_data = dict()
        line_plot_html = dict()
        for f in self.find_log_files('plot_vcf_dv_iframes/plot_vcf_dv_iframes_data'):
            self.plot_vcf_dv_iframes_plot_vcf_dv_iframes_data[f['s_name']] = dict()
            for l in f['f'].splitlines():
                key, value = l.split(None, 1)
                self.plot_vcf_dv_iframes_plot_vcf_dv_iframes_data[f['s_name']][key] = value
            for k, v in self.plot_vcf_dv_iframes_plot_vcf_dv_iframes_data[f['s_name']].items():
                line_plot_html[k] = ' <iframe src="' + v + '"   height="400px" width="100%" ></iframe> ' +'\n' 

        # Filter out samples matching ignored sample names
        #self.plot_vcf_dv_iframes_plot_vcf_dv_iframes_data = self.ignore_samples(self.plot_vcf_dv_iframes_plot_vcf_dv_iframes_data)

        log.info("Found {} reports".format(len(self.plot_vcf_dv_iframes_plot_vcf_dv_iframes_data)))

        if len(self.plot_vcf_dv_iframes_plot_vcf_dv_iframes_data) != 0:
            # Write parsed report data to a file
            self.write_data_file(self.plot_vcf_dv_iframes_plot_vcf_dv_iframes_data, 'multiqc_plot_vcf_dv_iframes')

            for k,v in line_plot_html.items():
            # Add a report section with the line plot
                self.add_section(
                    name = k+' VCF stats',
                    anchor = 'plot_vcf_dv_'+k,
                    description = 'This is a visual summary of vcf stats for '+k,
                    helptext = ''' ''',
                    plot = v
                )

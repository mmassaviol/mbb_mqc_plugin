#!/usr/bin/env python

""" mbb_mqc_plugin krona_iframe module """

from __future__ import print_function
from collections import OrderedDict
import logging

from multiqc import config
from multiqc.plots import linegraph
from multiqc.modules.base_module import BaseMultiqcModule

# Initialise the main MultiQC logger
log = logging.getLogger('multiqc')

class MultiqcModule(BaseMultiqcModule):

    def __init__(self):

        # Halt execution if we've disabled the plugin
        if config.kwargs.get('disable_plugin', True):
            return None

        # Initialise the parent module Class object
        super(MultiqcModule, self).__init__(
            name = 'Krona',
            target = "Krona",
            anchor = 'krona_iframe',
            href = 'https://github.com/marbl/Krona/wiki',
            info = "allows hierarchical data to be explored with zooming, multi-layered pie charts."
        )

        # Find and load any input files for this module
  
        self.krona_iframe_krona_data = dict()
        line_plot_html = ''
        for f in self.find_log_files('krona_iframe/krona_data'):
            self.krona_iframe_krona_data[f['s_name']] = dict()
            for l in f['f'].splitlines():
                key, value = l.split(None, 1)
                self.krona_iframe_krona_data[f['s_name']][key] = value
            for k, v in self.krona_iframe_krona_data[f['s_name']].items():
                line_plot_html = line_plot_html + ' <iframe src="' + v + '"   height="500px" width="100%" ></iframe> ' +'\n' 

        # Filter out samples matching ignored sample names
        #self.krona_iframe_krona_data = self.ignore_samples(self.krona_iframe_krona_data)

        log.info("Found {} reports".format(len(self.krona_iframe_krona_data)))

        if len(self.krona_iframe_krona_data) != 0:
            # Write parsed report data to a file
            self.write_data_file(self.krona_iframe_krona_data, 'multiqc_krona_iframe')


            # Add a report section with the line plot
            self.add_section(
                description = 'This plot is a Krona chart of OTU abundance per sample.',
                helptext = '''

                ''',
                plot = line_plot_html
            )
